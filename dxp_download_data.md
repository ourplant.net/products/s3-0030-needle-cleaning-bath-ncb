Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0030-needle-cleaning-bath-ncb).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         | [de](https://gitlab.com/ourplant.net/products/s3-0030-needle-cleaning-bath-ncb/-/raw/main/01_operating_manual/S3-0030_B_Betriebsanleitung.pdf)                  |
| assembly drawing         | [de](https://gitlab.com/ourplant.net/products/s3-0030-needle-cleaning-bath-ncb/-/raw/main/02_assembly_drawing/S3-0030_ZNB_A_Needle-Cleaning_Bath.pdf)                  |
| circuit diagram          |[de](https://gitlab.com/ourplant.net/products/s3-0030-needle-cleaning-bath-ncb/-/raw/main/03_circuit_diagram/30-0193-EPLAN-A.pdf)                   |
| maintenance instructions |                  |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0030-needle-cleaning-bath-ncb/-/raw/main/05_spare_parts/S3-0030-EVL-A.pdf)                   |

